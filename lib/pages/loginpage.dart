import 'package:flutter/material.dart';
import 'package:mhealth/pages/pagenavbar.dart';
import 'package:mhealth/source/source.dart';
import 'package:dropdown_search/dropdown_search.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  List<String> _listProvinsi = [
    'Jakarta',
    'Banten',
    'Jawa Barat',
    'Jawa Tengah',
    'Jawa Timur'
  ];
  final _res = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('images/logos.png'),
              SizedBox(
                height: 2,
              ),
              TextField(
                decoration: InputDecoration(
                    hintText: 'Masukkan Email',
                    icon: Icon(Icons.email),
                    border: OutlineInputBorder()),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                    hintText: 'Masukkan Password',
                    icon: Icon(Icons.vpn_key_outlined),
                    border: OutlineInputBorder()),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40),
                child: Container(
                  child: DropdownSearch<String>(
                    mode: Mode.MENU,
                    showSelectedItems: true,
                    items: _listProvinsi,
                    label: 'Pilih Provinsi',
                    hint: "Pilih Provinsi..",
                    onChanged: print,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 50,
                width: 200,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => PageNavbar()));
                  },
                  child: Text("LOGIN"),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(primaryBlack)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
