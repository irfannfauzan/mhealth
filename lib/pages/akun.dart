import 'package:flutter/material.dart';
import 'package:mhealth/pages/loginpage.dart';
import 'package:mhealth/source/source.dart';

class AkunPage extends StatefulWidget {
  @override
  _AkunPageState createState() => _AkunPageState();
}

class _AkunPageState extends State<AkunPage> {
  bool test = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: primaryBlack,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text("Account"),
            centerTitle: true,
          ),
          body: ListView(
            children: [
              Center(
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Container(
                        width: 130,
                        height: 130,
                        decoration: BoxDecoration(
                            border: Border.all(width: 4, color: Colors.white),
                            boxShadow: [
                              BoxShadow(
                                  spreadRadius: 2,
                                  blurRadius: 10,
                                  color: Colors.black.withOpacity(0.1))
                            ],
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage('images/acc.png'),
                                fit: BoxFit.cover)),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: customTextField("Full Name", "Irfan Fauzan R", false),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: customTextField("Email", "irfanfauzan@gmail.com", false),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: customTextField("Password", "*******", true),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: customTextField("Provinsi", "Jakarta", false),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50,
                    width: 200,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (_) => LoginPage()));
                      },
                      child: Text("Logout"),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(primaryBlack)),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              )
            ],
          )),
    );
  }
}

Widget customTextField(
    String labelText, String placeholder, bool isPasswordTextField) {
  bool obsecurePass = true;
  return Padding(
    padding: EdgeInsets.only(bottom: 30),
    child: TextField(
      obscureText: isPasswordTextField ? obsecurePass : false,
      decoration: InputDecoration(
          suffixIcon: isPasswordTextField
              ? IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: Colors.grey,
                  ))
              : null,
          contentPadding: EdgeInsets.only(bottom: 5),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: Colors.grey)),
    ),
  );
}
