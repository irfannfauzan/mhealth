import 'package:flutter/material.dart';
import 'package:mhealth/pages/akun.dart';
import 'package:mhealth/pages/help.dart';
import 'package:mhealth/pages/homepage.dart';
import 'package:mhealth/pages/screen/berita.dart';
import 'package:mhealth/pages/screen/beritaolahraga.dart';
import 'package:mhealth/pages/screen/detailreminder.dart';
import 'package:mhealth/pages/screen/reminder.dart';
import 'package:mhealth/source/source.dart';

class NavigationBar extends StatefulWidget {
  const NavigationBar({Key? key}) : super(key: key);

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  int _selectedItems = 0;
  final List<Widget> _children = [HomePage(), AkunPage(), HelpPage()];

  void onTappedBar(int index) {
    setState(() {
      _selectedItems = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[_selectedItems],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _selectedItems,
          selectedItemColor: Colors.white,
          backgroundColor: primaryBlack,
          items: [
            BottomNavigationBarItem(
                label: "Home", icon: new Icon(Icons.home_outlined)),
            BottomNavigationBarItem(
                label: "Account",
                icon: new Icon(Icons.account_circle_outlined)),
            BottomNavigationBarItem(
                label: "Help", icon: new Icon(Icons.help_outline)),
          ],
        ));
  }
}

class NavigationBarTitle extends StatefulWidget {
  const NavigationBarTitle({Key? key}) : super(key: key);

  @override
  _NavigationBarTitleState createState() => _NavigationBarTitleState();
}

class _NavigationBarTitleState extends State<NavigationBarTitle> {
  int _selectedItems = 0;
  final List<Widget> _children = [ReminderPage(), DetailsPage()];

  void onTappedBar(int index) {
    setState(() {
      _selectedItems = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[_selectedItems],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _selectedItems,
          selectedItemColor: Colors.white,
          backgroundColor: primaryBlack,
          items: [
            BottomNavigationBarItem(
                label: "Reminder", icon: new Icon(Icons.home)),
            BottomNavigationBarItem(
                label: "Details",
                icon: new Icon(Icons.account_circle_outlined)),
          ],
        ));
  }
}

class BeritaNavBar extends StatefulWidget {
  const BeritaNavBar({Key? key}) : super(key: key);

  @override
  _BeritaNavBarState createState() => _BeritaNavBarState();
}

class _BeritaNavBarState extends State<BeritaNavBar> {
  int _selectedItems = 0;
  final List<Widget> _children = [BeritaPage(), BeritaOlahragaPage()];

  void onTappedBar(int index) {
    setState(() {
      _selectedItems = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[_selectedItems],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _selectedItems,
          selectedItemColor: Colors.white,
          backgroundColor: primaryBlack,
          items: [
            BottomNavigationBarItem(
                label: "Health News",
                icon: new Icon(Icons.health_and_safety_outlined)),
            BottomNavigationBarItem(
                label: "Sport News", icon: new Icon(Icons.sports_bar_outlined)),
          ],
        ));
  }
}
