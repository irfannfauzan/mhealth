import 'package:flutter/material.dart';
import 'package:mhealth/pages/pagenavbar.dart';
import 'package:mhealth/source/source.dart';

import '../content.dart';

class BeritaOlahragaPage extends StatefulWidget {
  const BeritaOlahragaPage({Key? key}) : super(key: key);

  @override
  _BeritaOlahragaPageState createState() => _BeritaOlahragaPageState();
}

class _BeritaOlahragaPageState extends State<BeritaOlahragaPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: primaryBlack,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("mHealth News"),
          centerTitle: true,
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => PageNavbar()));
              },
              icon: Icon(Icons.arrow_back_outlined)),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: ListView.separated(
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        PopUpOlahraga(context);
                      },
                      child: NewWidget(
                        images: 'images/messi.jpeg',
                        title:
                            'Lihat Messi Rebahan di PSG, Bikin Miris atau Salut?',
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
              itemCount: 8),
        ),
      ),
    );
  }
}

class NewWidget extends StatelessWidget {
  final String images;
  final String title;

  const NewWidget({Key? key, required this.images, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.network(
          images,
          fit: BoxFit.cover,
          height: 100,
          width: 100,
        ),
      ),
      title: Text(title),
    );
  }
}
