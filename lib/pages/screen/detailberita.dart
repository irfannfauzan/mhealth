import 'package:flutter/material.dart';

class DetailBerita extends StatefulWidget {
  const DetailBerita({Key? key}) : super(key: key);

  @override
  _DetailBeritaState createState() => _DetailBeritaState();
}

class _DetailBeritaState extends State<DetailBerita> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: ListView(
            children: [
              Center(
                child: Container(
                  margin: EdgeInsets.all(12),
                  padding: EdgeInsets.all(8),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(25)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Icon(
                                  Icons.cancel,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 200,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/jokowi.jpeg'),
                                fit: BoxFit.fill),
                            borderRadius: BorderRadius.circular(20)),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Text(
                          "Jokowi Sudah Dijadwalkan, Bakal Ada Booster Vaksin COVID-19 di 2022?",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Container(
                        child: Text(
                            "Jakarta - Kementerian Kesehatan menyebut Presiden Joko Widodo (Jokowi) bakal menerima booster atau dosis ketiga vaksin COVID-19 pada 2022. Hal tersebut sudah dikonfirmasi oleh juru bicara vaksinasi COVID-19 Kemenkes, dr Siti Nadia Tarmizi Rencana (booster vaksin Presiden Jokowi) di 2022 ya ujar dr Nadia yang dikutip dari CNN Indonesia, Kamis (29/9/2021). Menteri Kesehatan Budi Gunadi Sadikin dalam rapat bersama Komisi IX DPR RI sempat menyebut, vaksin booster untuk masyarakat umum tersedia tahun. Akan tetapi, tak semua orang bakal mendapatkannya secara gratis. Baca artikel detikHealth, Jokowi Sudah Dijadwalkan, Bakal Ada Booster Vaksin COVID-19 di 2022? Ia menyebut berdasarkan studi terhadap tenaga kesehatan (nakes), imunitas dari vaksin COVID-19 berkurang dalam kurun waktu sekitar enam bulan. Studi tersebutlah yang mendasari pemberian booster untuk nakes. Terlebih mengingat, kelompok ini amat rentan tertular seiring meningkatnya kasus COVID-19 RI beberapa waktu lalu. Begitu juga perihal pemberian booster pada masyarakat, dr Prima menegaskan diperlukan studi lebih lanjut. Yakni, untuk menentukan kriteria masyarakat penerima dan waktu yang paling tepat untuk menerima suntikan booster"),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
