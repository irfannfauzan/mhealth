import 'package:flutter/material.dart';
import 'package:mhealth/pages/pagenavbar.dart';
import 'package:mhealth/source/source.dart';

import '../content.dart';

class BeritaPage extends StatefulWidget {
  const BeritaPage({Key? key}) : super(key: key);

  @override
  _BeritaPageState createState() => _BeritaPageState();
}

class _BeritaPageState extends State<BeritaPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: primaryBlack,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("mHealth News"),
          centerTitle: true,
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => PageNavbar()));
              },
              icon: Icon(Icons.arrow_back_outlined)),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: ListView.separated(
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        PopUpBerita(context);
                      },
                      child: NewWidget(
                        images: 'images/nakes.jpeg',
                        title:
                            'Sebaran 1.954 Kasus COVID-19 RI Per 29 September: Jatim 225, DKI 177',
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        PopUpBerita(context);
                      },
                      child: NewWidget(
                        images: 'images/jokowi.jpeg',
                        title:
                            'Jokowi Sudah Dijadwalkan, Bakal Ada Booster Vaksin COVID-19 di 2022?',
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        PopUpBerita(context);
                      },
                      child: NewWidget(
                        images: 'images/covidmenurun.jpeg',
                        title:
                            'COVID-19 Menurun di Indonesia, Di Mohon Prokes Jangan Sampai Kendor!',
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        PopUpBerita(context);
                      },
                      child: NewWidget(
                        images: 'images/pasiencovid.jpeg',
                        title:
                            'Kemenkes: Varian Lambda hingga R.1 Belum Terdeteksi di Indonesia',
                      ),
                    ),
                  ],
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
              itemCount: 2),
        ),
      ),
    );
  }
}

class NewWidget extends StatelessWidget {
  final String images;
  final String title;

  const NewWidget({Key? key, required this.images, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.network(
          images,
          fit: BoxFit.cover,
          height: 100,
          width: 100,
        ),
      ),
      title: Text(title),
    );
  }
}
