import 'package:flutter/material.dart';
import 'package:mhealth/source/source.dart';

class DetailBeritaOlahraga extends StatefulWidget {
  const DetailBeritaOlahraga({Key? key}) : super(key: key);

  @override
  _DetailBeritaOlahragaState createState() => _DetailBeritaOlahragaState();
}

class _DetailBeritaOlahragaState extends State<DetailBeritaOlahraga> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: primaryBlack,
        ),
        home: Scaffold(
          body: ListView(
            children: [
              Center(
                child: Container(
                  margin: EdgeInsets.all(12),
                  padding: EdgeInsets.all(8),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(25)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Icon(
                                  Icons.cancel,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 200,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/messi.jpeg'),
                                fit: BoxFit.fill),
                            borderRadius: BorderRadius.circular(20)),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Text(
                          "Lihat Messi Rebahan di PSG, Bikin Miris atau Salut?",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Container(
                        child: Text(
                            "Jakarta - Ada momen Lionel Messi rebahan di atas lapangan saat melapis pagar betis rekan-rekannya di Paris Saint-Germain. Salut atau bikin miris? Adegan itu tersaji di partai matchday kedua Liga Champions ketika PSG meraih kemenangan 2-0 atas Manchester City yang datang bertamu, Rabu (29/9) dini hari WIB. Messi mencetak gol kedua PSG, yang juga gol pertamanya di klub tersebut.Selepas mencetak gol itulah, peraih enam Ballon dOr tersebut sempat terlihat rebahan di lapangan dalam menahan tendangan bebas Man City. Tepatnya di masa injury time. Saat itu PSG dihukum tendangan bebas menyusul pelanggaran Georginio Wijnaldum terhadap Riyad Mahrez. Para pemain PSG pun berbaris membuat pagar betis, menghadapi tendangan dari Man City. Yang menarik, Lionel Messi ketika itu bukan ikut membantu dengan cara berdiri di samping rekan-rekan satu timnya. Ia justru rebahan di balik jejeran kaki para pemain PSG untuk mengantisipasi bola bawah tendangan bebas City. Melihat adegan salah satu pesepakbola terbaik dalam sejarah itu harus rebahan di atas lapangan sontak membuat publik terbagi dua. Ada yang merasa miris tak habis pikir bisa-bisanya Messi diperlakukan seperti itu di PSG."),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
