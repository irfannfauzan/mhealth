import 'package:flutter/material.dart';
import 'package:mhealth/pages/pagenavbar.dart';
import 'package:mhealth/source/source.dart';

class ReminderPage extends StatefulWidget {
  const ReminderPage({Key? key}) : super(key: key);

  @override
  _ReminderPageState createState() => _ReminderPageState();
}

class _ReminderPageState extends State<ReminderPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: primaryBlack,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text("Reminder"),
            centerTitle: true,
            leading: IconButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => PageNavbar()));
                },
                icon: Icon(Icons.arrow_back_outlined)),
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: Container(
                    height: 35,
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: primaryBlack),
                      borderRadius: BorderRadius.circular(29.5),
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: 'Search',
                          icon: Icon(Icons.search),
                          border: InputBorder.none),
                    ),
                  ),
                ),
                ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          DataTable(columns: [
                            DataColumn(
                                label: Text(
                              "No",
                              style: TextStyle(
                                  fontStyle: FontStyle.normal, fontSize: 12),
                            )),
                            DataColumn(
                                label: Text(
                              "Kelurahan",
                              style: TextStyle(
                                  fontStyle: FontStyle.normal, fontSize: 12),
                            )),
                            DataColumn(
                                label: Text(
                              "Rt/Rw",
                              style: TextStyle(
                                  fontStyle: FontStyle.normal, fontSize: 12),
                            )),
                            DataColumn(
                                label: Text(
                              "Nama",
                              style: TextStyle(
                                  fontStyle: FontStyle.normal, fontSize: 12),
                            )),
                          ], rows: [
                            DataRow(cells: [
                              DataCell(Text(
                                "1",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                textAlign: TextAlign.start,
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "2",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "3",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "4",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "5",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "6",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "8",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "9",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                            DataRow(cells: [
                              DataCell(Text(
                                "10",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Lebak",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "1/7",
                                maxLines: 2,
                              )),
                              DataCell(Text(
                                "Parna",
                                maxLines: 2,
                              )),
                            ]),
                          ]),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
