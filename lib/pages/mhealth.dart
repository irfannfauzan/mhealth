// import 'package:flutter/material.dart';
// import 'package:mhealth/pages/content.dart';
// import 'package:mhealth/pages/help.dart';
// import 'package:mhealth/pages/loginpage.dart';
// import 'package:mhealth/source/source.dart';

// class MHealth extends StatefulWidget {
//   const MHealth({Key? key}) : super(key: key);

//   @override
//   _MHealthState createState() => _MHealthState();
// }

// class _MHealthState extends State<MHealth> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SingleChildScrollView(
//         scrollDirection: Axis.vertical,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.end,
//           children: <Widget>[
//             ContentGreetings(),
//             Padding(
//               padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
//               child: Container(
//                 height: 35,
//                 padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
//                 decoration: BoxDecoration(
//                   color: Colors.white,
//                   border: Border.all(color: primaryBlack),
//                   borderRadius: BorderRadius.circular(29.5),
//                 ),
//                 child: TextField(
//                   decoration: InputDecoration(
//                       hintText: 'Search',
//                       icon: Icon(Icons.search),
//                       border: InputBorder.none),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: GestureDetector(
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: [
//                     Text(
//                       "News mHealth",
//                       style:
//                           TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
//                     ),
//                   ],
//                 ),
//                 onTap: () {
//                   print('tap');
//                   Navigator.of(context).pushReplacement(
//                       MaterialPageRoute(builder: (_) => LoginPage()));
//                 },
//               ),
//             ),
//             News(),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: GestureDetector(
//                 child: Text(
//                   "Show More..",
//                   style: TextStyle(fontWeight: FontWeight.bold),
//                 ),
//                 onTap: () {
//                   Navigator.of(context)
//                       .push(MaterialPageRoute(builder: (_) => HelpPage()));
//                 },
//               ),
//             ),
//             MainContentMenu(),
//           ],
//         ),
//       ),
//     );
//   }
// }
