import 'package:flutter/material.dart';
import 'package:mhealth/pages/pagenavbar.dart';
import 'package:mhealth/pages/screen/detailberita.dart';
import 'package:mhealth/pages/screen/detailberitaolahraga.dart';
import 'package:mhealth/source/newsimage.dart';
import 'package:mhealth/source/source.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ContentGreetings extends StatelessWidget {
  const ContentGreetings({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var hour = DateTime.now().hour;
    var mes = '';
    var imagess = '';
    if (hour < 11) {
      mes = 'Selamat Pagi,';
      print('Pagi Berhasil');
      imagess = 'images/sunrise.png';
    } else if (hour >= 11 && (hour < 15)) {
      mes = 'Selamat Siang,';
      print('Siang Berhasil');
      imagess = 'images/sun.png';
    } else if (hour >= 15 && (hour < 18)) {
      mes = 'Selamat Sore,';
      print('Sore Berhasil');
      imagess = 'images/sunset.png';
    } else if (hour >= 18 && (hour < 23)) {
      mes = 'Selamat Malam,';
      print('Malam Berhasil');
      imagess = 'images/malam.png';
    }
    return Padding(
      padding: const EdgeInsets.all(11),
      child: Container(
        height: 70,
        width: 380,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xFF0081A0),
        ),
        child: ListTile(
          title: Text(
            mes + " " + TextSource.name,
            style: TextStyle(
              color: defaultWhite,
            ),
          ),
          subtitle: Text(TextSource.subtitleGreetings,
              style: TextStyle(
                color: defaultWhite,
              )),
          leading: CircleAvatar(
            backgroundImage: AssetImage('images/acc.png'),
          ),
          trailing: CircleAvatar(
            backgroundImage: AssetImage(imagess),
          ),
        ),
      ),
    );
  }
}

// class MainContentMenu extends StatelessWidget {
//   const MainContentMenu({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(15),
//       child: GridView.count(
//         shrinkWrap: true,
//         physics: NeverScrollableScrollPhysics(),
//         crossAxisCount: 2,
//         children: <Widget>[
//           GestureDetector(
//             onTap: () => Navigator.of(context).pushReplacement(
//                 MaterialPageRoute(builder: (_) => PageNavbarDetail())),
//             child: ContentMenu(
//               title: "Jkn",
//               images: 'images/jkn1.png',
//             ),
//           ),
//           GestureDetector(
//             onTap: () {
//               Navigator.of(context).pushReplacement(
//                   MaterialPageRoute(builder: (_) => ReminderPage()));
//             },
//             child: ContentMenu(
//               title: "Keluarga Berencana",
//               images: 'images/keluargaberencana.png',
//             ),
//           ),
//           ContentMenu(
//             title: "Imunisasi",
//             images: 'images/imunisasi.png',
//           ),
//           GestureDetector(
//             onTap: () {
//               PopUpPage(context);
//             },
//             child: ContentMenu(
//               title: "More",
//               images: 'images/showmore.png',
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

class ContentMenu extends StatelessWidget {
  final String title;
  final String images;

  const ContentMenu({Key? key, required this.title, required this.images})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      child: InkWell(
        splashColor: primaryBlack,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image(
                image: AssetImage(images),
                height: 60,
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 14,
                    color: primaryBlack,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class FullContentMenu extends StatelessWidget {
  const FullContentMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 3,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Jkn",
              images: 'images/jkn1.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Keluarga Berencana",
              images: 'images/keluargaberencana.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Imunisasi",
              images: 'images/imunisasi.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Asi",
              images: 'images/asi.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Pertumbuhan Balita",
              images: 'images/pertumbuhanbalita.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "TBC",
              images: 'images/tbc.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Hipertensi",
              images: 'images/tbc.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Ibu Bersalin",
              images: 'images/ibubersalin.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Gangguan Jiwa",
              images: 'images/gangguanjiwa.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Keluarga Merokok",
              images: 'images/peroko.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Air Bersih",
              images: 'images/airbersih.png',
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => PageNavbarDetail()));
            },
            child: ContentMenu(
              title: "Jamban Sehat",
              images: 'images/jamban.png',
            ),
          ),
        ],
      ),
    );
  }
}

class FullContent extends StatelessWidget {
  final String title;
  final String images;

  const FullContent({Key? key, required this.title, required this.images})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      child: InkWell(
        splashColor: primaryBlack,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image(
                image: AssetImage(images),
                height: 35,
              ),
              Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 11,
                    color: primaryBlack,
                    fontWeight: FontWeight.bold),
              ),
              IconButton(onPressed: () {}, icon: Icon(Icons.cancel))
            ],
          ),
        ),
      ),
    );
  }
}

class News extends StatelessWidget {
  const News({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CarouselSlider.builder(
          itemCount: images1.length,
          itemBuilder: (context, index, realIndex) {
            final res = images1[index];
            final res2 = pesan[index];
            return buildImage(res, index, context, res2);
          },
          options: CarouselOptions(height: 200, autoPlay: true)),
    );
  }
}

Widget buildImage(String images, int index, BuildContext context, String res2) {
  return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      color: defaultWhite,
      child: Column(
        children: <Widget>[
          GestureDetector(
              onTap: () {
                PopUpBerita(context);
              },
              child: newsImage(imageNews: images, title: res2))
        ],
      ));
}

class newsImage extends StatelessWidget {
  final String imageNews;
  final String title;

  const newsImage({
    Key? key,
    required this.imageNews,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage(
              imageNews,
            ),
            fit: BoxFit.cover,
            height: 150,
            width: double.infinity,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: primaryBlack,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }
}

void PopUpPage(context) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (context) => Container(
      height: MediaQuery.of(context).size.height * 0.75,
      decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(25.0),
          topRight: const Radius.circular(25.0),
        ),
      ),
      child: Center(
        child: FullContentMenu(),
      ),
    ),
  );
}

void PopUpBerita(context) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (context) => Container(
      height: MediaQuery.of(context).size.height * 0.80,
      decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(25.0),
          topRight: const Radius.circular(25.0),
        ),
      ),
      child: Center(
        child: DetailBerita(),
      ),
    ),
  );
}

void PopUpOlahraga(context) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (context) => Container(
      height: MediaQuery.of(context).size.height * 0.80,
      decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(25.0),
          topRight: const Radius.circular(25.0),
        ),
      ),
      child: Center(
        child: DetailBeritaOlahraga(),
      ),
    ),
  );
}
