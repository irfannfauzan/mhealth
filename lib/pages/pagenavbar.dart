import 'package:flutter/material.dart';
import 'package:mhealth/pages/bottomnavbar.dart';
import 'package:mhealth/pages/homepage.dart';
import 'package:mhealth/pages/screen/berita.dart';
import 'package:mhealth/pages/screen/reminder.dart';
import 'package:mhealth/source/source.dart';

class PageNavbar extends StatefulWidget {
  const PageNavbar({Key? key}) : super(key: key);

  @override
  _PageNavbarState createState() => _PageNavbarState();
}

class _PageNavbarState extends State<PageNavbar> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: primaryBlack,
        ),
        home: Scaffold(
          body: HomePage(),
          bottomNavigationBar: NavigationBar(),
        ));
  }
}

class PageNavbarDetail extends StatefulWidget {
  const PageNavbarDetail({Key? key}) : super(key: key);

  @override
  _PageNavbarDetailState createState() => _PageNavbarDetailState();
}

class _PageNavbarDetailState extends State<PageNavbarDetail> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: primaryBlack,
        ),
        home: Scaffold(
          body: ReminderPage(),
          bottomNavigationBar: NavigationBarTitle(),
        ));
  }
}

class PageNavbarBerita extends StatefulWidget {
  const PageNavbarBerita({Key? key}) : super(key: key);

  @override
  _PageNavbarBeritaState createState() => _PageNavbarBeritaState();
}

class _PageNavbarBeritaState extends State<PageNavbarBerita> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: primaryBlack,
        ),
        home: Scaffold(
          body: BeritaPage(),
          bottomNavigationBar: BeritaNavBar(),
        ));
  }
}
