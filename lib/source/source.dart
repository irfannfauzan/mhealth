import 'package:flutter/material.dart';

Color primaryBlack = Color(0xff202c3b);
Color defaultWhite = Colors.white;

class TextSource {
  static String greetings = "Welcome, Irfan";
  static String name = "Irfan";
  static String avatarLeading = "https://picsum.photos/200";
  static String subtitleGreetings = "Nakes";
  static String avatarTrailing = "https://picsum.photos/200";
}
